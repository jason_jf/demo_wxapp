# 文件结构
模块标识 we7_wxappsample
## js
> 小程序源码目录
### 小程序源码文件目录结构
> 上传给微擎的源码压缩包目录结构
- we7_wxappsample //模块标识
- - we7 // [小程序官方类库](https://gitee.com/we7coreteam/wxapp/tree/master/we7)
- - we7_wxappsample 小程序源码
- - app.json
- - app.js
- - app.wxss
## php
> 小程序后台源码
#### 目录结构
- we7_wxappsample
- - template 视图模板目录
- - - mobile 微站模板目录
- - wxapp.php
- - manifest.xml
- - install.php
- - icon.jpg
- - preview.jpg
- - site.php
- - module.php

#### 文件说明
1. 小程序后台入口wxapp.php
2. install.php 安装脚本
3. manifest.xml 微擎开发者后台自动生成
4. module.php [文档](http://s.we7.cc/index.php?c=wiki&do=view&id=1&list=385) 
5. preview.jpg icon.jpg 微擎开发者设计模块 后台上传图片后 生成
6. site.php [说明文档](http://s.we7.cc/index.php?c=wiki&do=view&id=1&list=387)
## zip 
 打包上传的2个压缩包
> 上传给微擎的2个源码包 按这个目录结构上传
- js 小程序压缩包
- php 小程序后台压缩包

[免费购买小程序模块示例](http://s.we7.cc/module-9440.html)
 